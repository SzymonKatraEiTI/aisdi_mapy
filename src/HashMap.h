#ifndef AISDI_MAPS_HASHMAP_H
#define AISDI_MAPS_HASHMAP_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <utility>
#include <functional>
#include <algorithm>
#include <cstring>
#include "LinkedList.h"

namespace aisdi
{

	template <typename KeyType, typename ValueType>
	class HashMap
	{
	public:
		using key_type = KeyType;
		using mapped_type = ValueType;
		using value_type = std::pair<const key_type, mapped_type>;
		using size_type = std::size_t;
		using reference = value_type & ;
		using const_reference = const value_type&;

		class ConstIterator;
		class Iterator;
		using iterator = Iterator;
		using const_iterator = ConstIterator;
		friend class ConstIterator;

		static const size_t HASHTABLE_SIZE = 65536;

	private:
		size_t m_size;
		std::hash<key_type> m_hash;
		aisdi::LinkedList<value_type> m_baskets[HASHTABLE_SIZE];

		size_t getHash(const key_type& key) const
		{
			return m_hash(key) % HASHTABLE_SIZE;
		}
		void copyFrom(const HashMap& other)
		{
			m_size = other.m_size;
			m_hash = other.m_hash;
			for (size_t i = 0; i < HASHTABLE_SIZE; i++)
			{
				for (const value_type& el : other.m_baskets[i])
				{
					m_baskets[i].append(el);
				}
			}
		}
		void moveFrom(HashMap&& other)
		{
			m_size = std::move(other.m_size);
			m_hash = std::move(other.m_hash);
			for (size_t i = 0; i < HASHTABLE_SIZE; i++)
			{
				m_baskets[i] = std::move(other.m_baskets[i]);
			}
		}
		void clearBaskets()
		{
			for (size_t i = 0; i < HASHTABLE_SIZE; i++)
			{
				m_baskets[i].erase(m_baskets[i].begin(), m_baskets[i].end());
			}
		}
		const_iterator getGuard() const
		{
			return const_iterator(*this, 0, m_baskets[0].begin(), true);
		}

	public:

		HashMap()
			: m_size(0), m_hash()
		{}

		HashMap(std::initializer_list<value_type> list)
			: HashMap()
		{
			for (const value_type& i : list)
			{
				(*this)[i.first] = i.second;
			}
		}

		HashMap(const HashMap& other)
		{
			copyFrom(other);
		}

		HashMap(HashMap&& other)
		{
			moveFrom(std::move(other));
		}

		HashMap& operator=(const HashMap& other)
		{
			if (this != &other)
			{
				clearBaskets();
				copyFrom(other);
			}

			return *this;
		}

		HashMap& operator=(HashMap&& other)
		{
			clearBaskets();
			moveFrom(std::move(other));

			return *this;
		}

		bool isEmpty() const
		{
			return m_size == 0;
		}

		mapped_type& operator[](const key_type& key)
		{
			size_t hash = getHash(key);

			auto it = std::find_if(m_baskets[hash].begin(), m_baskets[hash].end(),
				[&key](const value_type& arg) {
				return arg.first == key;
			});

			if (it == m_baskets[hash].end()) // element with specified key not exists, add new
			{
				m_baskets[hash].append(value_type(key, mapped_type{})); // move rvalue into list

				m_size++;

				return m_baskets[hash].back().second;
			}
			else // overwrite if already exists
			{
				return (*it).second;
			}
		}

		const mapped_type& valueOf(const key_type& key) const
		{
			const_iterator it = find(key);

			if (it == end())
			{
				throw std::out_of_range("Element with specified key not found!");
			}

			return it->second;
		}

		mapped_type& valueOf(const key_type& key)
		{
			// ugly const_cast to avoid code duplication
			return const_cast<mapped_type&>(static_cast<const HashMap<KeyType, ValueType>*>(this)->valueOf(key));
		}

		const_iterator find(const key_type& key) const
		{
			size_t hash = getHash(key);

			auto it = std::find_if(m_baskets[hash].begin(), m_baskets[hash].end(),
				[&key](const value_type& arg) {
				return arg.first == key;
			});

			if (it != m_baskets[hash].end())
			{
				return const_iterator(*this, hash, it);
			}

			return getGuard(); // when not found
		}

		iterator find(const key_type& key)
		{
			return static_cast<const HashMap<KeyType, ValueType>*>(this)->find(key);
		}

		void remove(const key_type& key)
		{
			auto it = find(key);

			remove(it);
		}

		void remove(const const_iterator& it)
		{
			if (it == end())
			{
				throw std::out_of_range("Element not exists!");
			}

			m_baskets[it.m_currentBasket].erase(it.m_currentBasketIterator);

			m_size--;
		}

		size_type getSize() const
		{
			return m_size;
		}

		bool operator==(const HashMap& other) const
		{
			if (m_size != other.m_size)
			{
				return false;
			}

			for (size_t i = 0; i < HASHTABLE_SIZE; i++)
			{
				if (m_baskets[i].getSize() != other.m_baskets[i].getSize())
				{
					return false;
				}

				for (auto itA = m_baskets[i].begin(); itA != m_baskets[i].end(); itA++)
				{
					for (auto itB = other.m_baskets[i].begin(); itB != other.m_baskets[i].end(); itB++)
					{
						if (*itA != *itB)
						{
							return false;
						}
					}
				}
			}

			return true;
		}

		bool operator!=(const HashMap& other) const
		{
			return !(*this == other);
		}

		iterator begin()
		{
			if (m_size == 0)
			{
				return getGuard();
			}

			for (size_t i = 0; i < HASHTABLE_SIZE; i++)
			{
				if (m_baskets[i].getSize() == 0) continue;

				return iterator(*this, i, m_baskets[i].begin());
			}

			return getGuard(); // should not happen
		}

		iterator end()
		{
			return getGuard();
		}

		const_iterator cbegin() const
		{
			if (m_size == 0)
			{
				return getGuard();
			}

			for (size_t i = 0; i < HASHTABLE_SIZE; i++)
			{
				if (m_baskets[i].getSize() == 0) continue;

				return const_iterator(*this, i, m_baskets[i].begin());
			}

			return getGuard(); // should not happen
		}

		const_iterator cend() const
		{
			return getGuard();
		}

		const_iterator begin() const
		{
			return cbegin();
		}

		const_iterator end() const
		{
			return cend();
		}
	};

	template <typename KeyType, typename ValueType>
	class HashMap<KeyType, ValueType>::ConstIterator
	{
	public:
		using reference = typename HashMap::const_reference;
		using iterator_category = std::bidirectional_iterator_tag;
		using value_type = typename HashMap::value_type;
		using pointer = const typename HashMap::value_type*;
		friend class HashMap;

	private:
		const HashMap<KeyType, ValueType>& m_owner;
		size_t m_currentBasket;
		typename aisdi::LinkedList<value_type>::const_iterator m_currentBasketIterator;
		bool m_isGuard;

	public:

		explicit ConstIterator(const HashMap<KeyType, ValueType>& owner, size_t basket, typename aisdi::LinkedList<value_type>::const_iterator basketIterator, bool guard = false)
			: m_owner(owner), m_currentBasket(basket), m_currentBasketIterator(basketIterator), m_isGuard(guard)
		{}

		ConstIterator(const ConstIterator& other)
			: m_owner(other.m_owner), m_currentBasket(other.m_currentBasket), m_currentBasketIterator(other.m_currentBasketIterator), m_isGuard(other.m_isGuard)
		{
		}

		ConstIterator& operator++()
		{
			if (m_isGuard) throw std::out_of_range("No more elements in collection!");

			m_currentBasketIterator++;
			if (m_currentBasketIterator == m_owner.m_baskets[m_currentBasket].end())
			{
				bool found = false;
				for (size_t i = m_currentBasket + 1; i < HashMap::HASHTABLE_SIZE; i++)
				{
					if (m_owner.m_baskets[i].getSize() != 0)
					{
						m_currentBasket = i;
						m_currentBasketIterator = m_owner.m_baskets[i].begin();
						found = true;
						break;
					}
				}

				if (!found) m_isGuard = true;
			}

			return *this;
		}

		ConstIterator operator++(int)
		{
			ConstIterator tmp(*this);

			++(*this);

			return tmp;
		}

		ConstIterator& operator--()
		{
			if (!m_isGuard)
			{
				if (m_currentBasketIterator != m_owner.m_baskets[m_currentBasket].begin())
				{
					--m_currentBasketIterator;
					return *this;
				}
			}

			bool found = false;
			int startPos = static_cast<int>(m_isGuard ? HashMap::HASHTABLE_SIZE - 1 : m_currentBasket - 1);
			for (int i = startPos; i >= 0; i--)
			{
				if (m_owner.m_baskets[i].getSize() != 0)
				{
					m_currentBasket = static_cast<size_t>(i);
					m_currentBasketIterator = --m_owner.m_baskets[i].end();
					m_isGuard = false;
					found = true;
					break;
				}
			}

			if (!found) throw std::out_of_range("No more elements in collection!");

			return *this;
		}

		ConstIterator operator--(int)
		{
			ConstIterator tmp(*this);

			--(*this);

			return tmp;
		}

		reference operator*() const
		{
			if (m_isGuard) throw std::out_of_range("Not a valid collection item!");

			return *m_currentBasketIterator;
		}

		pointer operator->() const
		{
			return &this->operator*();
		}

		bool operator==(const ConstIterator& other) const
		{
			if (m_isGuard != other.m_isGuard)
			{
				return false;
			}

			return (m_currentBasket == other.m_currentBasket && m_currentBasketIterator == other.m_currentBasketIterator) ||
				(m_isGuard);
		}

		bool operator!=(const ConstIterator& other) const
		{
			return !(*this == other);
		}
	};

	template <typename KeyType, typename ValueType>
	class HashMap<KeyType, ValueType>::Iterator : public HashMap<KeyType, ValueType>::ConstIterator
	{
	public:
		using reference = typename HashMap::reference;
		using pointer = typename HashMap::value_type*;

		explicit Iterator(const HashMap<KeyType, ValueType>& owner, size_t basket, typename aisdi::LinkedList<value_type>::iterator basketIterator, bool guard = false)
			: ConstIterator(owner, basket, basketIterator, guard)
		{}

		Iterator(const ConstIterator& other)
			: ConstIterator(other)
		{}

		Iterator& operator++()
		{
			ConstIterator::operator++();
			return *this;
		}

		Iterator operator++(int)
		{
			auto result = *this;
			ConstIterator::operator++();
			return result;
		}

		Iterator& operator--()
		{
			ConstIterator::operator--();
			return *this;
		}

		Iterator operator--(int)
		{
			auto result = *this;
			ConstIterator::operator--();
			return result;
		}

		pointer operator->() const
		{
			return &this->operator*();
		}

		reference operator*() const
		{
			// ugly cast, yet reduces code duplication.
			return const_cast<reference>(ConstIterator::operator*());
		}
	};

}

#endif /* AISDI_MAPS_HASHMAP_H */
