#ifndef AISDI_LINEAR_LINKEDLIST_H
#define AISDI_LINEAR_LINKEDLIST_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>

namespace aisdi
{

template <typename Type>
class LinkedList
{
public:
  using difference_type = std::ptrdiff_t;
  using size_type = std::size_t;
  using value_type = Type;
  using pointer = Type*;
  using reference = Type&;
  using const_pointer = const Type*;
  using const_reference = const Type&;

  class ConstIterator;
  class Iterator;
  using iterator = Iterator;
  using const_iterator = ConstIterator;

private:
  class Node
  {
  public:
    Node* previous;
    Node* next;

    virtual bool hasValue() const
    {
      return false;
    }

    virtual ~Node()
    { }
  };

  class ValueNode : public Node
  {
  public:
    Type value;

    ValueNode(const_reference item) : value(item)
    {

    }

    virtual bool hasValue() const override
    {
      return true;
    }

    virtual ~ValueNode()
    { }
  };

  size_type m_size;
  Node* m_first;
  Node* m_last;
  Node* m_guard;

  void initialize()
  {
    m_size = 0;

    m_guard = new Node();
    m_guard->previous = m_guard;
    m_guard->next = m_guard;

    m_first = m_last = m_guard;
  }
  void copyFrom(const LinkedList& other)
  {
    initialize();

    for (iterator i = other.begin(); i != other.end(); i++)
    {
      append(*i);
    }
  }
  void moveFrom(LinkedList&& other)
  {
    m_size = other.m_size;
    m_first = other.m_first;
    m_last = other.m_last;
    m_guard = other.m_guard;

    other.m_size = 0;
    other.m_first = nullptr;
    other.m_last = nullptr;
    other.m_guard = nullptr;
  }
  void freeAll()
  {
    if (m_first != nullptr)
    {
      erase(begin(), end());
    }

    if (m_guard != nullptr) delete m_guard;

    m_first = m_last = m_guard = nullptr;
  }

public:
  LinkedList()
  {
    initialize();
  }

  LinkedList(std::initializer_list<Type> l)
  {
    initialize();
    for (const Type& i : l)
    {
      append(i);
    }
  }

  LinkedList(const LinkedList& other)
  {
    copyFrom(other);
  }

  LinkedList(LinkedList&& other)
  {
    moveFrom(std::move(other));
  }

  ~LinkedList()
  {
    freeAll();
  }

  LinkedList& operator=(const LinkedList& other)
  {
    if (this != &other)
    {
      freeAll();
      copyFrom(other);
    }

    return *this;
  }

  LinkedList& operator=(LinkedList&& other)
  {
    freeAll();
    moveFrom(std::move(other));
    return *this;
  }

  bool isEmpty() const
  {
    return m_size == 0;
  }

  size_type getSize() const
  {
    return m_size;
  }

  void append(const Type& item)
  {
    ValueNode* value = new ValueNode(item);

    value->next = m_guard;
    m_guard->previous = value;

    value->previous = m_last;
    m_last->next = value;

    if (m_size == 0)
    {
      m_first = value;
    }
    m_last = value;

    m_size++;
  }

  void prepend(const Type& item)
  {
    ValueNode* value = new ValueNode(item);

    value->next = m_first;
    m_first->previous = value;

    value->previous = m_guard;
    m_guard->next = value;

    if (m_size == 0)
    {
      m_last = value;
    }
    m_first = value;

    m_size++;
  }

  void insert(const const_iterator& insertPosition, const Type& item)
  {
    ValueNode* value = new ValueNode(item);

    Node* insertPlace = insertPosition.getBasePointer();

    value->previous = insertPlace->previous;
    insertPlace->previous->next = value;

    value->next = insertPlace;
    insertPlace->previous = value;

    if (insertPlace == m_first)
    {
      m_first = value;
    }
    if (insertPlace == m_last)
    {
      m_last = value;
    }

    m_size++;
  }

  Type popFirst()
  {
    if (m_size == 0) throw std::logic_error("Collection is empty");

    ValueNode* item = static_cast<ValueNode*>(m_first);
    Type tmp = item->value;

    m_first->next->previous = m_first->previous;
    m_first->previous->next = m_first->next;

    if (m_last == m_first)
    {
      m_last = m_first->next; // if there is only one element, then m_first->next points to guard
    }
    m_first = m_first->next;

    delete item;

    m_size--;

    return tmp;
  }

  Type popLast()
  {
    if (m_size == 0) throw std::logic_error("Collection is empty");

    ValueNode* item = static_cast<ValueNode*>(m_last);
    Type tmp = item->value;

    m_last->previous->next = m_last->next;
    m_last->next->previous = m_last->previous;

    if (m_first == m_last)
    {
      m_first = m_last->previous; // if there is only one element, then m_last->previous points to guard
    }
    m_last = m_last->previous;

    delete item;

    m_size--;

    return tmp;
  }

  Type& back()
  {
	  if (m_size == 0) throw std::logic_error("Collection is empty");

	  ValueNode* item = static_cast<ValueNode*>(m_last);

	  return item->value;
  }

  void erase(const const_iterator& possition)
  {
    Node* erasePlace = possition.getBasePointer();

    if (!erasePlace->hasValue()) throw std::out_of_range("Tried to erase non existing element");

    erasePlace->next->previous = erasePlace->previous;
    erasePlace->previous->next = erasePlace->next;

    if (erasePlace == m_first)
    {
      m_first = erasePlace->next;
    }
    if (erasePlace == m_last)
    {
      m_last = erasePlace->previous;
    }

    delete erasePlace;

    m_size--;
  }

  void erase(const const_iterator& firstIncluded, const const_iterator& lastExcluded)
  {
    Node* eraseFirst = firstIncluded.getBasePointer();
    Node* eraseLast = lastExcluded.getBasePointer();

    if (m_first == eraseFirst)
    {
      m_first = eraseLast;
    }
    if (m_last == eraseLast)
    {
      m_last = eraseFirst->previous;
    }

    eraseFirst->previous->next = eraseLast;
    eraseLast->previous = eraseFirst->previous;

    Node* current = eraseFirst;
    size_t erasedCount = 0;
    while (current != eraseLast)
    {
      Node* next = current->next;

      delete current;

      current = next;

      erasedCount++;
    }

    m_size -= erasedCount;
  }

  iterator begin()
  {
    return iterator(m_first);
  }

  iterator end()
  {
    return iterator(m_guard);
  }

  const_iterator cbegin() const
  {
    return const_iterator(m_first);
  }

  const_iterator cend() const
  {
    return const_iterator(m_guard);
  }

  const_iterator begin() const
  {
    return cbegin();
  }

  const_iterator end() const
  {
    return cend();
  }
};

template <typename Type>
class LinkedList<Type>::ConstIterator
{
friend class LinkedList<Type>;

public:
  using iterator_category = std::bidirectional_iterator_tag;
  using value_type = typename LinkedList::value_type;
  using difference_type = typename LinkedList::difference_type;
  using pointer = typename LinkedList::const_pointer;
  using reference = typename LinkedList::const_reference;

private:
  Node* m_current;

  Node* getBasePointer() const
  {
    return m_current;
  }

public:
  explicit ConstIterator(Node* ptr) : m_current(ptr)
  {

  }

  reference operator*() const
  {
    if (!m_current->hasValue()) throw std::out_of_range("You cannot get value from guard element");

    ValueNode* curr = static_cast<ValueNode*>(m_current);

    return curr->value;
  }

  ConstIterator& operator++()
  {
    if (!m_current->hasValue()) throw std::out_of_range("Cannot move iterator after guard element");

    m_current = m_current->next;

    return *this;
  }

  ConstIterator operator++(int)
  {
    ConstIterator tmp(m_current);

    ++(*this);

    return tmp;
  }

  ConstIterator& operator--()
  {
    if (!m_current->previous->hasValue()) throw std::out_of_range("Cannot move iterator before first element");

    m_current = m_current->previous;

    return *this;
  }

  ConstIterator operator--(int)
  {
    ConstIterator tmp(m_current);

    --(*this);

    return tmp;
  }

  ConstIterator operator+(difference_type d) const
  {
    ConstIterator it(m_current);

    while (d--)
    {
      ++it;
    }

    return it;
  }

  ConstIterator operator-(difference_type d) const
  {
    ConstIterator it(m_current);

    while(d--)
    {
      --it;
    }

    return it;
  }

  bool operator==(const ConstIterator& other) const
  {
    return m_current == other.m_current;
  }

  bool operator!=(const ConstIterator& other) const
  {
    return !ConstIterator::operator==(other);
  }
};

template <typename Type>
class LinkedList<Type>::Iterator : public LinkedList<Type>::ConstIterator
{
public:
  using pointer = typename LinkedList::pointer;
  using reference = typename LinkedList::reference;

  explicit Iterator(Node* ptr) : ConstIterator(ptr)
  {}

  Iterator(const ConstIterator& other)
    : ConstIterator(other)
  {}

  Iterator& operator++()
  {
    ConstIterator::operator++();
    return *this;
  }

  Iterator operator++(int)
  {
    auto result = *this;
    ConstIterator::operator++();
    return result;
  }

  Iterator& operator--()
  {
    ConstIterator::operator--();
    return *this;
  }

  Iterator operator--(int)
  {
    auto result = *this;
    ConstIterator::operator--();
    return result;
  }

  Iterator operator+(difference_type d) const
  {
    return ConstIterator::operator+(d);
  }

  Iterator operator-(difference_type d) const
  {
    return ConstIterator::operator-(d);
  }

  reference operator*() const
  {
    // ugly cast, yet reduces code duplication.
    return const_cast<reference>(ConstIterator::operator*());
  }
};

}

#endif // AISDI_LINEAR_LINKEDLIST_H
