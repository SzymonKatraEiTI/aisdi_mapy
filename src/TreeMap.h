#ifndef AISDI_MAPS_TREEMAP_H
#define AISDI_MAPS_TREEMAP_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <utility>
#include <vector>
#include <tuple>

namespace aisdi
{

	template <typename KeyType, typename ValueType>
	class TreeMap
	{
	public:
		using key_type = KeyType;
		using mapped_type = ValueType;
		using value_type = std::pair<const key_type, mapped_type>;
		using size_type = std::size_t;
		using reference = value_type & ;
		using const_reference = const value_type&;

		class ConstIterator;
		class Iterator;
		using iterator = Iterator;
		using const_iterator = ConstIterator;

	private:
		class Node
		{
		public:
			enum class Type
			{
				Data,
				Sentinel,
				LeafSentinel
			};

			Node* parent;
			Node* left;
			Node* right;
			bool isRed; // true - red; false - black
			Type type;

			Node(Type type, bool isRed = false)
				: parent(nullptr), left(nullptr), right(nullptr), isRed(isRed), type(type)
			{
			}

			virtual ~Node() { }

		};
		class ValueNode : public Node
		{
		public:
			value_type data;

			ValueNode(const key_type& key, const mapped_type& value, bool isRed = false)
				: Node(Node::Type::Data, isRed), data(key, value)
			{
			}
		};

		Node m_leafSentinel;
		Node m_sentinel;
		Node* m_root;
		size_t m_size;

		// returns inserted/found node, bool indicates it, true - new node, false - found node with the same key
		std::tuple<ValueNode*, bool> insertOrFindKey(const key_type& key)
		{
			Node* current = m_root;
			while (true) // breaks are inside loop
			{
				ValueNode* currentValue = static_cast<ValueNode*>(current);
				if (key < currentValue->data.first)
				{
					if (current->left == &m_leafSentinel)
					{
						ValueNode* newNode = new ValueNode(key, mapped_type{}, true); // every new node is red
						newNode->left = newNode->right = &m_leafSentinel;

						current->left = newNode;
						newNode->parent = current;
						m_size++;

						return std::make_tuple(newNode, true);
					}
					else
					{
						current = current->left;
					}
				}
				else if (key > currentValue->data.first)
				{
					if (current->right == &m_leafSentinel)
					{
						ValueNode* newNode = new ValueNode(key, mapped_type{}, true); // every new node is red
						newNode->left = newNode->right = &m_leafSentinel;

						current->right = newNode;
						newNode->parent = current;
						m_size++;

						return std::make_tuple(newNode, true);
					}
					else
					{
						current = current->right;
					}
				}
				else
				{
					return std::make_tuple(currentValue, false); // found the same value
				}
			}
		}

		void balanceTree(Node* current)
		{
			while (true) // breaks inside loop
			{
				Node *parent = current->parent;
				Node *grandparent = parent->parent;
				if (grandparent->type == Node::Type::Sentinel)
				{
					break;
				}
				Node *uncle = nullptr;
				bool uncleOnRight;
				if (grandparent->left == parent)
				{
					uncle = grandparent->right;
					uncleOnRight = true;
				}
				else
				{
					uncle = grandparent->left;
					uncleOnRight = false;
				}

				if (uncle->isRed)
				{
					parent->isRed = false;
					uncle->isRed = false;
					grandparent->isRed = true;

					if (grandparent == m_root)
					{
						grandparent->isRed = false;
						break;
					}
					else
					{
						current = grandparent;
					}
				}
				else // uncle is black
				{
					if (uncleOnRight)
					{
						if (current == parent->right)
						{
							leftRotation(current);
							current = parent;
						}
						else if (current == parent->left)
						{
							rightRotation(parent);
							grandparent->isRed = !grandparent->isRed;
							parent->isRed = !parent->isRed;
							break;
						}
					}
					else
					{
						if (current == parent->left)
						{
							rightRotation(current);
							current = parent;
						}
						else if (current == parent->right)
						{
							leftRotation(parent);
							grandparent->isRed = !grandparent->isRed;
							parent->isRed = !parent->isRed;
							break;
						}
					}
				}
			}
		}

		void fixColors(Node* node)
		{
			while (true) // break inside loop
			{
				Node* parent = node->parent;
				if (parent->type == Node::Type::Sentinel)
				{
					break;
				}
				Node* brother = nullptr;
				bool brotherOnRight;
				if (parent->left == node)
				{
					brother = parent->right;
					brotherOnRight = true;
				}
				else
				{
					brother = parent->left;
					brotherOnRight = false;
				}

				if (brother->isRed)
				{
					if (brotherOnRight)
					{
						leftRotation(brother);
					}
					else
					{
						rightRotation(brother);
					}

					bool tmpColor = parent->isRed; // exchange colors
					parent->isRed = brother->isRed;
					brother->isRed = tmpColor;

					// will be resolved in next step
				}
				else
				{
					Node* nephewLeft = brother->left;
					Node* nephewRight = brother->right;

					if (brotherOnRight)
					{
						if (!nephewLeft->isRed && !nephewRight->isRed)
						{
							// node has correct color now
							brother->isRed = true;
							node = parent;
							// resolve one level up in next step
						}
						else if (nephewLeft->isRed && !nephewRight->isRed)
						{
							rightRotation(nephewLeft);

							bool tmpColor = nephewLeft->isRed; // exchange colors
							nephewLeft->isRed = brother->isRed;
							brother->isRed = tmpColor;
							// will be resolved in next step
						}
						else if (nephewRight->isRed)
						{
							leftRotation(brother);
							brother->isRed = parent->isRed;
							parent->isRed = false;
							nephewRight->isRed = false;
							break;
						}
					}
					else
					{
						if (!nephewLeft->isRed && !nephewRight->isRed)
						{
							// node has correct color now
							brother->isRed = true;
							node = parent;
							// resolve one level up in next step
						}
						else if (!nephewLeft->isRed && nephewRight->isRed)
						{
							leftRotation(nephewRight);

							bool tmpColor = nephewRight->isRed; // exchange colors
							nephewRight->isRed = brother->isRed;
							brother->isRed = tmpColor;
							// will be resolved in next step
						}
						else if (nephewLeft->isRed)
						{
							rightRotation(brother);
							brother->isRed = parent->isRed;
							parent->isRed = false;
							nephewLeft->isRed = false;
							break;
						}
					}
				}
			}
		}

		void leftRotation(Node* node)
		{
			Node* parent = node->parent;
			Node* grandparent = parent->parent;

			if (grandparent->left == parent)
			{
				grandparent->left = node;
			}
			if (grandparent->right == parent)
			{
				grandparent->right = node;
			}
			node->parent = grandparent;

			Node* tmp = node->left;

			node->left = parent;
			parent->parent = node;
			parent->right = tmp;
			if (tmp->type != Node::Type::LeafSentinel)
			{
				tmp->parent = parent;
			}

			if (parent == m_root)
			{
				m_root = node;
			}
		}

		void rightRotation(Node* node)
		{
			Node* parent = node->parent;
			Node* grandparent = parent->parent;

			if (grandparent->left == parent)
			{
				grandparent->left = node;
			}
			if (grandparent->right == parent)
			{
				grandparent->right = node;
			}
			node->parent = grandparent;

			Node* tmp = node->right;

			node->right = parent;
			parent->parent = node;
			parent->left = tmp;
			if (tmp->type != Node::Type::LeafSentinel)
			{
				tmp->parent = parent;
			}

			if (parent == m_root)
			{
				m_root = node;
			}
		}

		Node* findMin(Node* node) const
		{
			Node* min = node;
			while (min->left->type != Node::Type::LeafSentinel)
			{
				min = min->left;
			}
			return min;
		}

		void clearTree()
		{
			if (m_root != nullptr && m_root != &m_sentinel)
			{
				std::vector<Node*> toDelete;
				for (auto it = begin(); it != end(); it++)
				{
					toDelete.push_back(it.getBaseCurrent());
				}

				for (auto node : toDelete)
				{
					delete node;
				}

				m_root = &m_sentinel;
				m_size = 0;
			}
		}

		void copyFrom(const TreeMap& other)
		{
			for (auto it = other.begin(); it != other.end(); it++)
			{
				(*this)[it->first] = it->second;
			}
		}

		void moveFrom(TreeMap&& other)
		{
			m_size = other.m_size;

			m_root = other.m_root;
			// replace sentinel with one from current instance
			if (other.m_root != &other.m_sentinel)
			{
				m_root->parent = &m_sentinel;
				m_sentinel.left = m_sentinel.right = m_root;
			}
			else
			{
				m_root = &m_sentinel;
			}
			other.m_root = nullptr;

			// search for leaf sentinels and replace with ones from current instance
			for (auto it = begin(); it != end(); it++)
			{
				Node* current = it.getBaseCurrent();
				if (current->left == &other.m_leafSentinel)
				{
					current->left = &m_leafSentinel;
				}
				if (current->right == &other.m_leafSentinel)
				{
					current->right = &m_leafSentinel;
				}
			}
		}

	public:
		TreeMap()
			: m_leafSentinel(Node::Type::LeafSentinel), m_sentinel(Node::Type::Sentinel), m_root(&m_sentinel), m_size(0)
		{
			m_sentinel.left = m_sentinel.right = &m_leafSentinel;
		}

		TreeMap(std::initializer_list<value_type> list)
			: TreeMap()
		{
			for (const value_type& i : list)
			{
				(*this)[i.first] = i.second;
			}
		}

		TreeMap(const TreeMap& other)
			: TreeMap()
		{
			copyFrom(other);
		}

		TreeMap(TreeMap&& other)
			: TreeMap()
		{
			moveFrom(std::move(other));
		}

		~TreeMap()
		{
			clearTree();
		}

		TreeMap& operator=(const TreeMap& other)
		{
			if (this != &other)
			{
				clearTree();
				copyFrom(other);
			}

			return *this;
		}

		TreeMap& operator=(TreeMap&& other)
		{
			clearTree();
			moveFrom(std::move(other));

			return *this;
		}

		bool isEmpty() const
		{
			return m_size == 0;
		}

		mapped_type& operator[](const key_type& key)
		{
			ValueNode* node;

			if (m_size == 0)
			{
				node = new ValueNode(key, mapped_type{}, false); // root node is black
				node->left = node->right = &m_leafSentinel;

				m_root = node;
				m_root->parent = &m_sentinel;
				m_sentinel.left = m_sentinel.right = node;

				m_size++;
			}
			else
			{
				std::tuple<ValueNode*, bool> result = insertOrFindKey(key);
				node = std::get<0>(result);

				if (std::get<1>(result)) // new node
				{
					if (node->parent->isRed)
					{
						balanceTree(node);
					}
				}
			}

			return node->data.second;
		}

		const mapped_type& valueOf(const key_type& key) const
		{
			const_iterator it = find(key);

			if (it == end())
			{
				throw std::out_of_range("Element with specified key not found!");
			}

			return it->second;
		}

		mapped_type& valueOf(const key_type& key)
		{
			// ugly const_cast to avoid code duplication
			return const_cast<mapped_type&>(static_cast<const TreeMap<KeyType, ValueType>*>(this)->valueOf(key));
		}

		const_iterator find(const key_type& key) const
		{
			Node* current = m_root;
			while (current->type == Node::Type::Data)
			{
				ValueNode* currentValue = static_cast<ValueNode*>(current);
				if (key < currentValue->data.first)
				{
					current = current->left;
				}
				else if (key > currentValue->data.first)
				{
					current = current->right;
				}
				else
				{
					return const_iterator(*this, current);
				}
			}

			return const_iterator(*this, &m_sentinel); // not found
		}

		iterator find(const key_type& key)
		{
			return static_cast<const TreeMap<KeyType, ValueType>*>(this)->find(key);
		}

		void remove(const key_type& key)
		{
			auto it = find(key);

			remove(it);
		}

		void remove(const const_iterator& it)
		{
			if (it == end())
			{
				throw std::out_of_range("Element not exists!");
			}

			// based on algorithm description from http://eduinf.waw.pl/inf/alg/001_search/0121.php

			Node* node = it.getBaseCurrent();
			Node* parent = node->parent;

			if (node->left->type == Node::Type::Data && node->right->type == Node::Type::Data)
			{
				Node* potentialSuccessor = node->right;
				bool successorIsRed;
				Node* nodeToFix = nullptr;

				if (potentialSuccessor->left->type == Node::Type::LeafSentinel)
				{
					// potentialSuccessor is valid successor now
					successorIsRed = potentialSuccessor->isRed;
					nodeToFix = potentialSuccessor->right;

					if (parent->left == node)
					{
						parent->left = potentialSuccessor;
					}
					if (parent->right == node)
					{
						parent->right = potentialSuccessor;
					}
					potentialSuccessor->parent = parent;

					potentialSuccessor->left = node->left;
					node->left->parent = potentialSuccessor;

					potentialSuccessor->isRed = node->isRed;

					if (node == m_root)
					{
						m_root = potentialSuccessor;
					}
				}
				else
				{
					Node* left = node->left;
					Node* right = node->right;

					const_iterator succIt(it);
					succIt++;
					Node* successor = succIt.getBaseCurrent();
					successorIsRed = successor->isRed;
					nodeToFix = successor->right;

					Node* successorParent = successor->parent;
					Node* successorRight = successor->right;

					successorParent->left = successorRight;
					if (successorRight->type == Node::Type::Data) // modify only if node has data, do not touch sentinel
					{
						successorRight->parent = successorParent;
						successorRight->isRed = successor->isRed;
					}

					if (parent->left == node)
					{
						parent->left = successor;
					}
					if (parent->right == node)
					{
						parent->right = successor;
					}
					successor->parent = parent;

					if (m_root == node)
					{
						m_root = successor;
					}

					successor->left = left;
					left->parent = successor;

					successor->right = right;
					right->parent = successor;

					successor->isRed = node->isRed;
				}

				if (!successorIsRed && nodeToFix->type != Node::Type::LeafSentinel)
				{
					fixColors(nodeToFix);
				}

				delete node;
			}
			else // one or zero childs
			{
				Node* child;

				if (node->left->type == Node::Type::LeafSentinel)
				{
					child = node->right;
				}
				else
				{
					child = node->left;
				}

				if (!node->isRed)
				{
					child->isRed = false;
				}

				if (parent->left == node)
				{
					parent->left = child;
				}
				if (parent->right == node)
				{
					parent->right = child;
				}

				if (child->type != Node::Type::LeafSentinel) // occurs in case when node has child
				{
					child->parent = parent;
				}

				if (node == m_root)
				{
					if (child->type != Node::Type::LeafSentinel)
					{
						m_root = child;
					}
					else
					{
						m_root = &m_sentinel;
					}
				}

				delete node;
				m_size--;
			}
		}

		size_type getSize() const
		{
			return m_size;
		}

		bool operator==(const TreeMap& other) const
		{
			if (m_size != other.m_size)
			{
				return false;
			}

			for (auto itA = cbegin(), itB = other.cbegin(); itA != cend(); itA++, itB++)
			{
				if (*itA != *itB)
				{
					return false;
				}
			}

			return true;
		}

		bool operator!=(const TreeMap& other) const
		{
			return !(*this == other);
		}

		iterator begin()
		{
			return iterator(*this, findMin(m_root));
		}

		iterator end()
		{
			return iterator(*this, &m_sentinel);
		}

		const_iterator cbegin() const
		{
			return const_iterator(*this, findMin(m_root));
		}

		const_iterator cend() const
		{
			return const_iterator(*this, &m_sentinel);
		}

		const_iterator begin() const
		{
			return cbegin();
		}

		const_iterator end() const
		{
			return cend();
		}
	};

	template <typename KeyType, typename ValueType>
	class TreeMap<KeyType, ValueType>::ConstIterator
	{
	private:
		const TreeMap& m_owner;
		const Node* m_current;

		Node* getBaseCurrent() const
		{
			return const_cast<Node*>(m_current);
		}

	public:
		using reference = typename TreeMap::const_reference;
		using iterator_category = std::bidirectional_iterator_tag;
		using value_type = typename TreeMap::value_type;
		using pointer = const typename TreeMap::value_type*;
		friend class TreeMap;

		explicit ConstIterator(const TreeMap& owner, const Node* current)
			: m_owner(owner), m_current(current)
		{}

		ConstIterator(const ConstIterator& other)
			: m_owner(other.m_owner), m_current(other.m_current)
		{
		}

		ConstIterator& operator++()
		{
			if (*(this) == m_owner.end())
			{
				throw std::out_of_range("No more elements in tree!");
			}

			if (m_current->right->type != Node::Type::LeafSentinel)
			{
				const Node* min = m_current->right;
				while (min->left->type != Node::Type::LeafSentinel)
				{
					min = min->left;
				}
				m_current = min;
				return *this;
			}

			const Node* x = m_current;
			const Node* y = m_current->parent;
			while (y->type != Node::Type::Sentinel && x == y->right)
			{
				x = y;
				y = y->parent;
			}

			m_current = y;
			return *this;
		}

		ConstIterator operator++(int)
		{
			ConstIterator tmp(*this);

			++(*this);

			return tmp;
		}

		ConstIterator& operator--()
		{
			if ((*this) == m_owner.begin())
			{
				throw std::out_of_range("No more elements in tree!");
			}

			if (m_current->left->type != Node::Type::LeafSentinel)
			{
				const Node* max = m_current->left;
				while (max->right->type != Node::Type::LeafSentinel)
				{
					max = max->right;
				}
				m_current = max;
				return *this;
			}

			const Node* x = m_current;
			const Node* y = m_current->parent;
			while (y->type != Node::Type::Sentinel && x == y->left)
			{
				x = y;
				y = y->parent;
			}

			m_current = y;
			return *this;
		}

		ConstIterator operator--(int)
		{
			ConstIterator tmp(*this);

			--(*this);

			return tmp;
		}

		reference operator*() const
		{
			if (m_current->type != Node::Type::Data)
			{
				throw std::out_of_range("This iterator does not point to node with data");
			}

			const ValueNode* val = static_cast<const ValueNode*>(m_current);

			return val->data;
		}

		pointer operator->() const
		{
			return &this->operator*();
		}

		bool operator==(const ConstIterator& other) const
		{
			return m_current == other.m_current;
		}

		bool operator!=(const ConstIterator& other) const
		{
			return !(*this == other);
		}
	};

	template <typename KeyType, typename ValueType>
	class TreeMap<KeyType, ValueType>::Iterator : public TreeMap<KeyType, ValueType>::ConstIterator
	{
	public:
		using reference = typename TreeMap::reference;
		using pointer = typename TreeMap::value_type*;

		explicit Iterator(const TreeMap& owner, Node* current)
			: ConstIterator(owner, current)
		{}

		Iterator(const ConstIterator& other)
			: ConstIterator(other)
		{}

		Iterator& operator++()
		{
			ConstIterator::operator++();
			return *this;
		}

		Iterator operator++(int)
		{
			auto result = *this;
			ConstIterator::operator++();
			return result;
		}

		Iterator& operator--()
		{
			ConstIterator::operator--();
			return *this;
		}

		Iterator operator--(int)
		{
			auto result = *this;
			ConstIterator::operator--();
			return result;
		}

		pointer operator->() const
		{
			return &this->operator*();
		}

		reference operator*() const
		{
			// ugly cast, yet reduces code duplication.
			return const_cast<reference>(ConstIterator::operator*());
		}
	};

}

#endif /* AISDI_MAPS_MAP_H */
