#include <cstddef>
#include <cstdlib>
#include <string>
#include <iostream>
#include <chrono>
#include <functional>
#include <cstdio>
#include <ctime>

#include "TreeMap.h"
#include "HashMap.h"

void measure(const std::string& name, std::function<void()> action)
{
	using namespace std::chrono;

	high_resolution_clock::time_point start, end;

	start = high_resolution_clock::now();

	action();

	end = high_resolution_clock::now();
	std::cout << name << " - " << duration_cast<duration<double>>(end - start).count() << " s" << std::endl;
}

int main(int argc, char** argv)
{
	using namespace std::chrono;

	srand(time(NULL));

	size_t testCount = argc > 1 ? atoi(argv[1]) : 1000000;

	aisdi::TreeMap<int, int> tree;
	aisdi::HashMap<int, int> hash;

	std::cout << "INSERT RANDOM" << std::endl;
	measure("TreeMap", [&testCount, &tree]()
	{
		for (size_t i = 0; i < testCount; i++)
		{
			tree[rand()] = rand();
		}
	});
	measure("HashMap", [&testCount, &hash]()
	{
		for (size_t i = 0; i < testCount; i++)
		{
			hash[rand()] = rand();
		}
	});
	std::cout << std::endl;

	std::cout << "FIND" << std::endl;
	measure("TreeMap", [&testCount, &tree]()
	{
		for (size_t i = 0; i < testCount; i++)
		{
			tree.find(rand());
		}
	});
	measure("HashMap", [&testCount, &hash]()
	{
		for (size_t i = 0; i < testCount; i++)
		{
			hash.find(rand());
		}
	});
	std::cout << std::endl;

	aisdi::TreeMap<int, int> tree2;
	aisdi::HashMap<int, int> hash2;

	std::cout << "INSERT SEQUENTIAL" << std::endl;
	measure("TreeMap", [&testCount, &tree2]()
	{
		for (size_t i = 0; i < testCount; i++)
		{
			tree2[i] = rand();
		}
	});
	measure("HashMap", [&testCount, &hash2]()
	{
		for (size_t i = 0; i < testCount; i++)
		{
			hash2[i] = rand();
		}
	});
	std::cout << std::endl;

	std::cout << "REMOVE SEQUENTIAL EVERY 5" << std::endl;
	measure("TreeMap", [&testCount, &tree2]()
	{
		for (size_t i = 0; i < testCount; i += 5)
		{
			tree2.remove(i);
		}
	});
	measure("HashMap", [&testCount, &hash2]()
	{
		for (size_t i = 0; i < testCount; i+= 5)
		{
			hash2.remove(i);
		}
	});
	std::cout << std::endl;

	return 0;
}
